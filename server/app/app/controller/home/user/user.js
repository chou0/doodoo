const base = require("./../base");
const moment = require("moment");
const { Payment, Notification } = require("easy-alipay");

module.exports = class extends base {
    async _initialize() {
        await super.isAppAuth();
    }

    /**
     *
     * @api {get} /home/user/user/index 获取应用列表
     * @apiDescription 获取应用列表
     * @apiGroup App Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {String} keyword 关键词
     * @apiParam {Number} page 页码
     *
     * @apiSuccess {Array} success 应用列表
     *
     * @apiSampleRequest /home/user/user/index
     *
     */
    async index() {
        const { page = 1, keyword = "" } = this.query;
        const appId = this.state.app.id;

        let user = {
            data: [],
            pagination: {
                page: 1,
                pageSize: 20,
                rowCount: 0,
                pageCount: 1
            }
        };

        const app = await this.model("app")
            .query(qb => {
                qb.where("id", "=", appId);
            })
            .fetch({ withRelated: ["wxa"] });
        console.log(app);
        if (app && app.wxa && app.wxa.id) {
            user = await this.model("user")
                .query(qb => {
                    qb.where("wxa_id", "=", app.wxa.id);
                    qb.orderBy("id", "desc");

                    if (keyword) {
                        qb.where("nickName", "like", `%${keyword}%`);
                    }
                })
                .fetchPage({ page: page, pageSize: 20 });
        }
        this.success(user);
    }
    /**
     *
     * @api {get} /home/user/user/recharge 应用充值
     * @apiDescription 获取应用列表
     * @apiGroup App Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} years 年限
     * @apiParam {String} redirect
     *
     * @apiSuccess {Array} success 应用列表
     *
     * @apiSampleRequest /home/user/user/recharge
     *
     */
    async recharge() {
        let { years } = this.query;
        const { redirect } = this.query;
        const tradeid = moment().format("YYYYMMDDhms");
        const template = await this.model("template")
            .query({
                where: {
                    id: this.state.app.template_id
                }
            })
            .fetch();
        console.log("years", years);
        console.log("template", template);
        let money = 0;
        if (Number(years) === 2) {
            money = template.price * 2;
            years = Number(years) + 1;
        } else {
            money = template.price;
            years = 1;
        }
        if (!money) {
            this.fail("参数错误");
            return;
        }

        const trade = await this.model("app_trade")
            .forge({
                app_id: this.state.app.id,
                custom_id: this.state.app.custom_id,
                tradeid: tradeid,
                money: money,
                payment: "支付宝",
                years: years,
                type: 0
            })
            .save();

        const notifyUrl = process.env.APP_HOST + "/app/pay/alipay/notify";
        const showUrl = process.env.APP_HOST;
        const returnUrl =
            process.env.APP_HOST +
            `/app/pay/alipay/return?redirect=${encodeURIComponent(redirect)}`;

        const url = await Payment.createDirectPay(
            process.env.ALIPAY_PARTNER,
            process.env.ALIPAY_KEY,
            process.env.ALIPAY_ACCOUNT,
            trade.tradeid,
            trade.tradeid,
            trade.money,
            trade.tradeid,
            showUrl,
            notifyUrl,
            returnUrl
        );

        this.success(url);
    }
};
