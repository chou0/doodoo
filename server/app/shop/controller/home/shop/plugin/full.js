const base = require("./../../base");

module.exports = class extends base {
    async _initialize() {
        await super.isCustomAuth();
        await super.isAppAuth();
        await super.isShopAuth();
    }

    /**
     *
     * @api {get} /shop/home/shop/plugin/full/index 满减列表
     * @apiDescription 满减列表
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} page 页码
     *
     * @apiSampleRequest /shop/home/shop/plugin/full/index
     *
     */
    async index() {
        const shopId = this.state.shop.id;
        const { page = 1 } = this.query;
        const fullcut = await this.model("fullcut")
            .query(qb => {
                qb.where("shop_id", shopId);
                qb.orderBy("id", "desc");
            })
            .fetchPage({
                pageSize: 20,
                page: page
            });
        this.success(fullcut);
    }

    /**
     *
     * @api {post} /shop/home/shop/plugin/full/add 新增/修改满减
     * @apiDescription 新增/修改满减
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} id            满减id
     * @apiParam {Number} price         满减价格
     * @apiParam {Number} limit_price   满减使用价格
     * @apiParam {Number} free_delivery 是否免配送费
     * @apiParam {Number} status        活动状态（1:开启，0：关闭）
     *
     * @apiSampleRequest /shop/home/shop/plugin/full/add
     *
     */
    async add() {
        const shopId = this.state.shop.id;
        const { id, price, limit_price, free_delivery, status = 1 } = this.post;

        const fullcut = await this.model("fullcut")
            .forge(
                Object.assign(
                    {
                        id,
                        price,
                        limit_price,
                        free_delivery,
                        status
                    },
                    {
                        shop_id: shopId
                    }
                )
            )
            .save();
        this.success(fullcut);
    }

    /**
     *
     * @api {get} /shop/home/shop/plugin/full/info 满减详情
     * @apiDescription 满减详情
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} id   满减id
     *
     * @apiSampleRequest /shop/home/shop/plugin/full/info
     *
     */
    async info() {
        const { id } = this.query;
        const shopId = this.state.shop.id;
        const fullcut = await this.model("fullcut")
            .query(qb => {
                qb.where("id", id);
                qb.where("shop_id", shopId);
            })
            .fetch();
        this.success(fullcut);
    }

    /**
     *
     * @api {get} /shop/home/shop/plugin/full/update 修改满减状态
     * @apiDescription 修改满减状态
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} id      满减id
     * @apiParam {Number} status  活动状态（1:开启，0：关闭）
     *
     * @apiSampleRequest /shop/home/shop/plugin/full/update
     *
     */
    async update() {
        const { id, status } = this.query;
        const fullcut = await this.model("fullcut")
            .forge({
                id,
                status
            })
            .save();
        this.success(fullcut);
    }

    /**
     *
     * @api {get} /shop/home/shop/plugin/full/del 删除满减
     * @apiDescription 删除满减
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} id      满减id
     *
     * @apiSampleRequest /shop/home/shop/plugin/full/del
     *
     */
    async del() {
        const { id } = this.query;
        const shopId = this.state.shop.id;
        const fullcut = await this.model("fullcut")
            .query(qb => {
                qb.where("id", id);
            })
            .destroy();
        this.success(fullcut);
    }
};
