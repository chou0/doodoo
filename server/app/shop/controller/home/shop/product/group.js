const moment = require("moment");
const base = require("./../../base");

module.exports = class extends base {
    async _initialize() {
        await super.isCustomAuth();
        await super.isAppAuth();
        await super.isShopAuth();
    }

    /**
     *
     * @api {get} /shop/home/shop/product/group/index 分组列表
     * @apiDescription 分组列表
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     *
     * @apiParam {Number} name   分组名称
     *
     * @apiSampleRequest /shop/home/shop/product/group/index
     *
     */
    async index() {
        const shopId = this.state.shop.id;
        const { name } = this.query;
        const productgroup = await this.model("productgroup")
            .query(qb => {
                qb.where("shop_id", shopId);
                if (this.isSet(name)) {
                    qb.where("name", "like", "%" + name + "%");
                }
                qb.orderBy("id", "desc");
            })
            .fetchAll();
        this.success(productgroup);
    }

    /**
     *
     * @api {post} /shop/home/shop/product/group/add 新增/修改分组
     * @apiDescription 新增/修改分组
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     *
     * @apiSampleRequest /shop/home/shop/product/group/add
     *
     */
    async add() {
        const shopId = this.state.shop.id;
        const data = this.post ? this.post : [];
        const group = [];
        if (data.length) {
            for (let i = 0; i < data.length; i++) {
                const productgroup = await this.model("productgroup")
                    .query(qb => {
                        qb.where("shop_id", shopId);
                        qb.where("name", data[i]);
                    })
                    .fetch();
                if (!productgroup) {
                    const obj = { shop_id: shopId, name: data[i] };
                    group.push(obj);
                }
            }
        }
        const productGroup = await doodoo.bookshelf.Collection.extend({
            model: this.model("productgroup")
        })
            .forge(group)
            .invokeThen("save");
        this.success(productGroup);
    }

    /**
     *
     * @api {get} /shop/home/shop/product/group/info 分组详情
     * @apiDescription 分组详情
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} id     分组id
     *
     * @apiSampleRequest /shop/home/shop/product/group/info
     *
     */
    async info() {
        const { id } = this.query;
        const shopId = this.state.shop.id;
        const productgroup = await this.model("productgroup")
            .query(qb => {
                qb.where("id", id);
                qb.where("shop_id", shopId);
            })
            .fetch();
        this.success(productgroup);
    }

    /**
     *
     * @api {get} /shop/home/shop/product/group/update 修改分组状态
     * @apiDescription 修改分组状态
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} id     分组id
     * @apiParam {Number} status 状态（1:开启 0：关闭）
     *
     * @apiSampleRequest /shop/home/shop/product/group/update
     *
     */
    async update() {
        const { id, status } = this.query;
        const productgroup = await this.model("productgroup")
            .forge({
                id,
                status
            })
            .save();
        this.success(productgroup);
    }

    /**
     *
     * @api {get} /shop/home/shop/product/group/del 删除分组
     * @apiDescription 删除分组
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} id     分组id
     *
     * @apiSampleRequest /shop/home/shop/product/group/del
     *
     */
    async del() {
        const { id } = this.query;
        const shopId = this.state.shop.id;
        const productgroup = await this.model("productgroup")
            .query(qb => {
                qb.where("id", id);
            })
            .destroy();
        this.success(productgroup);
    }
};
