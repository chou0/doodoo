const product_album = require("./product_album");
const product_group = require("./productgroup_product");
const product_sku = require("./productsku");
const collection = (module.exports = doodoo.bookshelf.Model.extend({
    tableName: "shop_user_collection",
    hasTimestamps: true
}));
module.exports = doodoo.bookshelf.Model.extend({
    tableName: "shop_product",
    hasTimestamps: true,
    softDelete: true,
    album: function() {
        return this.hasMany(product_album, "product_id");
    },
    group: function() {
        return this.hasMany(product_group, "product_id");
    },
    sku: function() {
        return this.hasMany(product_sku, "product_id");
    },
    collection: function() {
        return this.hasOne(collection, "collection_id");
    }
});
