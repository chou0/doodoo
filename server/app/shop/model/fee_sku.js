const fee_sku_address = require("./fee_sku_address");

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "shop_deliveryfee_sku",
    hasTimestamps: true,
    address: function() {
        return this.hasMany(fee_sku_address, "deliveryfee_sku_id");
    }
});
