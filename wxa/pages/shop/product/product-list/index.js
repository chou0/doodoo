Page({
    data: {
        color: wx.getExtConfigSync().color,
        rankList: ["综合", "销量", "价格"],
        show_type: 0, // 0：块状 1：列表
        orderType: 0, // 0："desc"降序   1："asc"升序
        orderBy: 'rank', // "rank":综合 "sale":销量 "price":价格
        selectIndex: 0,
        search_text: '',
        product_list: [],
        is_all: 0,
        page: 1
    },
    onLoad: function(options) {
        if (options.text) {
            this.setData({
                search_text: options.text
            })
        }
    },
    onShow: function() {
        this._clear();
        this.getProducts();
    },
    _clear() {
        this.setData({
            page: 1,
            product_list: [],
            is_all: 0,
            show_type: 0,
            orderType: 0,
            orderBy: 'rank'
        })
    },
    bindconfirm(e) {
        this.data.search_text = e.detail.value;
        this._clear();
        this.getProducts();
    },
    getProducts() {
        let product_list = this.data.product_list;
        wx.doodoo.fetch(`/shop/api/shop/search/index`,{
            method: "GET",
            data: {
                page: this.data.page,
                orderBy: this.data.orderBy,
                orderType: this.data.orderType ? 'asc' : 'desc',
                keyword: this.data.search_text
            }
        }).then(res => {
            if (res && res.data.errmsg=='ok') {
                this.data.page++;
                const DATA = res.data.data.data;
                product_list = product_list.concat(DATA);
                if (!DATA.length || !product_list.length) this.data.is_all = 1;
                this.setData({
                    page: this.data.page,
                    is_all: this.data.is_all,
                    product_list: product_list,
                    orderType: this.data.orderType
                });
            }
        })
    },
    // 切换商品展示状态
    switch_type(e) {
        const type = e.currentTarget.dataset.type;
        // show_type 0：块状 1：列表
        this.setData({
            show_type: type
        });
    },
    changeRank(e) {
        const index = Number(e.currentTarget.dataset.index);
        const type = Number(e.currentTarget.dataset.order_type);
        this.setData({
            page: 1,
            product_list: [],
            is_all: 0,
            selectIndex: index,
            orderBy: index==0 ? 'rank' : index==1 ? 'sale' : 'price',
            orderType: this.data.selectIndex == index ? (type ? 0 : 1) : type
        })
        this.getProducts();
    },
    goProductDetail(e) {
        wx.navigateTo({
            url: `/pages/shop/product/product-detail/index?id=${e.currentTarget.dataset.id}`
        });
    },
    onPullDownRefresh: function() {
        setTimeout(() => {
            this._clear();
            this.getProducts();
            wx.stopPullDownRefresh();
        }, 500)
    },
    onReachBottom: function() {
        if (!this.data.is_all) {
            this.getProducts();
        }
    }
})