// pages/order/order-detail/index.js
const util = require('../../../../utils/util.js')
const app = getApp();
Page({
    data: {
        color: wx.getExtConfigSync ? wx.getExtConfigSync().color : {}
    },
    onLoad(params) {
        if (params.order_id) {
            this.setData({
                order_id: Number(params.order_id)
            })
        }
    },
    onShow() {
        this.getOrderDetail();
    },
    getOrderDetail() {
        wx.doodoo.fetch(`/shop/api/shop/order/order/info?id=${this.data.order_id}`).then(res => {
            if (res && res.data.errmsg == 'ok') {
                const order_info = res.data.data;
                order_info.created_at = util.formatTime(order_info.created_at);
                this.setData({
                    order_info: order_info
                })
            }
        })
    },
    // 确认收货
    confirm_receipt(e) {
        const order_id = e.detail.value.order_id;
        wx.showModal({
            title: "提示",
            content: "确定收货吗？",
            success: function(res) {
                if (res.confirm) {
                    wx.doodoo.fetch(`/shop/api/shop/order/order/update`, {
                        method: 'GET',
                        data: {
                            id: order_id,
                            status: 2
                        }
                    }).then(res => {
                        if (res && res.data.errmsg == 'ok') {
                            wx.showToast({
                                title: "确认收货成功"
                            });
                            this.data.order_id = order_id;
                            this.getOrderDetail();
                        }
                    })
                } else if (res.cancel) {
                    console.log("用户点击取消");
                }
            }
        });
    },
    // 查询快递
    logistics(e) {
        const order = e.currentTarget.dataset.data;
        wx.navigateTo({
            url: `../logistics/index?order=${JSON.stringify(order)}`
        });
    },
    // 付款
    pay_order(e) {
        // payment_id ： 0余额 1微信 2支付宝 3货到
        // 微信支付
        this.data.order_id = e.currentTarget.dataset.id;
        wx.doodoo.fetch(`/app/pay/wxpay/index?id=${e.detail.value.trade_id}`).then(res => {
            if (res && res.data.errmsg == 'ok') {
                let response = res.data.data;
                let timeStamp = response.timeStamp;
                //可以支付
                wx.requestPayment({
                    timeStamp: timeStamp.toString(),
                    nonceStr: response.nonceStr,
                    package: response.package,
                    signType: response.signType,
                    paySign: response.paySign,
                    success: () => {
                        this.send_tplmsg(response.package, e.currentTarget.dataset.id); // 支付成功，发送支付成功模板
                        this.getOrderDetail();
                    },
                    fail: () => {
                        this.getOrderDetail();
                    }
                });
            } else {
                wx.showModal({
                    title: "提示",
                    content: res.data.data,
                    showCancel: !1
                });
            }
        });
    },
    // 取消订单
    cancelOrder: function(e) {
        const order_id = e.detail.value.order_id;
        const that = this;
        wx.showModal({
            title: "提示",
            content: "确定取消订单吗？",
            success: function(res) {
                if (res.confirm) {
                    wx.doodoo.fetch(`/shop/api/shop/order/order/update`, {
                        method: 'GET',
                        data: {
                            id: order_id,
                            status: -1
                        }
                    }).then(res => {
                        if (res && res.data.errmsg == 'ok') {
                            that.data.order_id = order_id;
                            that.getOrderDetail();
                        }
                    })
                } else if (res.cancel) {
                    console.log("用户点击取消");
                }
            }
        })
    },
    // 退款申请 申请售后
    backGoods(e) {
        const data = e.currentTarget.dataset.data;
        const type = e.currentTarget.dataset.type;
        wx.navigateTo({
            url: `../backGoods/index?data=${JSON.stringify(data)}&type=${type}`
        });
    },
    // 跳转到商品详情
    goProduct(e) {
        const product_id = e.currentTarget.dataset.product_id;
        wx.navigateTo({
            url: `/pages/shop/product/product-detail/index?id=${product_id}`
        });
    },
    // 发送模板消息
    send_tplmsg: function(formId, order_id) {
        formId = formId.substring(10);
        wx.doodoo.fetch("/api/order/sendPayTpl", {
            method: "GET",
            data: {
                formId: formId,
                orderId: order_id
            }
        }, res => {
            console.log("发送模板消息", res);
        });
    }
});