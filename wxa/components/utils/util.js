function px2rpx(num) {
    try {
        const res = wx.getSystemInfoSync();
        return (num * (750 / res.windowWidth)).toFixed(2);
        // return ((res.windowWidth * num * 2)/375).toFixed(2);
    } catch (e) {
        // return (750 * num / 320).toFixed(2);
    }
}

module.exports = {
    px2rpx
}